# Operadores
Sequelize proporciona una gran cantidad de operadores que nos ayudan a crear consultas complejas.
Estos operadores se encuentran disponibles dentro del objeto `Op`, que se puede importar desde `sequelize`
```
Para obtener mas información sobre operadores consulte la documentación oficial
https://sequelize.org/docs/v7/querying/operators/
```
## Operador Like
El operador `Like` nos permite buscar coincidencias dentro de una búsqueda y combinarlo con el operador `%` talcual como se haría con MySQL

Modfiquemos ahora nuestro método `getAllActorsController` para que cuando se le envíe un `name` haga uso de este operador.

> `./src/controllers/actors/getAllActorsController.js`

Importemos el operdor `Op` desde `sequelize`

```javascript
import { Op } from 'sequelize';
```
Y luego modifiquemos el `where` del `findAll()` que pusimos dentro del condicional

```javascript
const actorsName = await Actors.findAll({
    attributes: ['first_name','last_name'],
    where:{
        first_name: {
            [Op.like]: `%${name}%`
        }
    }
});
```
De esta menera ahora cada vez que se busque un nombre simplemente con que coincidan algunas letras el resultado será verdadero. Podemos buscar por el nombre `Johnny` o simplemente `Joh`.

## Algunos de los operadores más importantes

```javascript
import { Op } from 'sequelize';

Model.findAll({
    where:{
        [Op.and]: [{a: 5}, {b: 6}], //(a = 5) AND (b = 6)
        [Op.or]: [{a: 5}, {b: 6}], //(a = 5) OR (b = 6)

        someAttribute: {
            [Op.not]: true, //is not true
            [Op.between]: [5, 10], // between 5 and 10
            [Op.in]: [1, 2], //IN [1, 2]
            [Op.notIn]: [1, 2], //NOT IN [1, 2]
        }
    }
})
```
```
Listado completo de operadores: https://sequelize.org/docs/v6/core-concepts/model-querying-basics/#operators
```

# Order, Limit y Offset
Muchas veces necesitamos que una consulta a una tabla se muestre ordenada por un determinado criterio.
En otras ocacines vamos a necesitar limitar o restringir los registros que nos devuelve una consulta.
Para esto `sequelize` nos ofrece las `clausulas`: *`order`*, *`limt`* y *`offset`*

## Order
La opción de ordenamiento en `sequelize` toma una serie de elementos en forma de array recibiendo dos parámetros: el `campo` por el cual se la quiere ordenar y la `dirección` por la cual se lo quiere mostrar (`ASC` o `DESC`).

> `./src/controllers/actors/getAllActorsController.js`

```javascript
const actors = await Actors.findAll({
    attributes: ['first_name','last_name'],
    order: [
        ['last_name', 'DESC']
    ]
});
```
## Limit y Offset
Las cláusulas `limit` y `offset` se usan para restringir los registros que retorna una consulta `SELECT`.

La cláusula `limit` recibe un argumento numérico positivo que indica el número máximo de registros a retornar; mientras que `offset` indica el número del primer registro a retornar. `El número de registro inicial es 0 (no 1)`.

Por ejemplo, la siguiente consulta nos mostrará los 5 primeros registros:

> `./src/controllers/actors/getAllActorsController.js`

```javascript
const actors = await Actors.findAll({
    attributes: ['actor_id','first_name','last_name'],
    limit: 5,
    offset: 0
});

res.send({
    status: 'ok',
    data: actors
});
```
Pero si le cambiamos el `offset` a 5:

> `./src/controllers/actors/getAllActorsController.js`

```javascript
const actors = await Actors.findAll({
    attributes: ['actor_id','first_name','last_name'],
    limit: 5,
    offset: 5
});
```
Recuperamos cinco registros a partir del registro numero `5`, es decir nos devolverá del `6` hasta el `10`
```
Desde luego que, tanto el `limit` como el `offset`, deben ser pasados como parámetros al método para que funcione correctamente en el caso de querer hacer una paginación.
```

# Relaciones (Asociaciones) en Sequelize
Las relaciones en `Sequelize`, mas conocidas con el nombre de `associations`, existen para optimizar la obtención de datos.

`Sequelize` soporta las asociaciones estandar:

        👉 One-To-One: uno a uno
        👉 One-To-Many: una a muchos
        👉 Many-To-Many: muchos a muchos

Para esto `Sequelize` proporciona cuatro tipos de asociaciones que combinadas nos permiten crear las relaciones estandar:
- La asociación `HasOne` (Tiene uno) (uno a uno 1:1)
- La asociación `BelonsTo` (Pertenece a) (uno a muchos 1:N)
- La asociación `HasMany` (Tiene muchos/muchas) (uno a muchos 1:N)
- La asociación `BelongsToMany` (Pertenece a muchos/muchas) (muchos a muchos N:M)

Para entender como funcionan estas asociaciónes tomaremos las tablas `address` y `customer` de la base de datos `sakila` y armaremos los modelos y sincronización correspondientes en nuestro proyecto ejemplo.
Para faciliar el seguimiento de este ejemplo, les facilitamos el códido de los mismos:

> `./src/database/models/Address.js`
```javascript
import { DataTypes, Model, Sequelize } from "sequelize";
import connection from "../config/configDB.js";


class Address extends Model {};

Address.init({
    address_id:{
        type: DataTypes.SMALLINT.UNSIGNED,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
    },
    address:{
        type: DataTypes.STRING,
        allowNull: false
    },
    address2:{
        type: DataTypes.STRING,
        defaultValue: null
    },
    district:{
        type: DataTypes.STRING,
        allowNull: false
    },
    city_id:{
        type: DataTypes.SMALLINT.UNSIGNED,
        allowNull: false,
    },
    postal_code:{
        type: DataTypes.STRING,
        defaultValue: null
    },
    phone:{
        type: DataTypes.STRING,
        allowNull: false
    },
    locatios:{
        type: DataTypes.GEOMETRY,
    },
    last_update:{
        type: DataTypes.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP')
    }
    },
    {
        tableName: 'address',
        timestamps: false,
        sequelize: connection
    }
)

export default Address;

```

> `./src/database/models/Customer.js`

```javascript
import { DataTypes, Model } from "sequelize";
import connection from "../config/configDB.js";


class Customer extends Model {};

Customer.init({
    customer_id:{
        type: DataTypes.INTEGER.UNSIGNED,
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    store_id: {
        type: DataTypes.TINYINT.UNSIGNED,
        allowNull: true
    },
    first_name:{
        type: DataTypes.STRING,
        allowNull: true
    },
    last_name:{
        type: DataTypes.STRING,
        allowNull: true
    },
    email:{
        type: DataTypes.STRING
    },
    address_id:{
        type: DataTypes.SMALLINT.UNSIGNED,
        allowNull: true
    },
    active:{
        type: DataTypes.TINYINT,
        allowNull: true,
        defaultValue: 1
    },
    create_date:{
        type: DataTypes.DATE
    },
    last_update:{
        type: DataTypes.DATE
    }
    },
    {
        tableName: 'customer',
        timestamps: false,
        sequelize: connection
    }
)

export default Customer;
```
Una vez armados los modelos a la carpeta `models`, los agregaremos a nuestra función `syncDB` en `servicesDB.js` de la carpeta `services`.

> `./src/database/services/servicesDB.js`

```javascript
import Customer from '../models/Customer.js';
import Address from '../models/Address.js';
...

export const syncDB = async () => {
    
    await Actors.sync({alter: true});
    await Customer.sync({alter: true});
    await Address.sync({alter: true});
}
```
Hecho esto una vez que actualize el archivo `serviceDB.js` la sincronización de los nuevos modelos debería haberse realizado en forma correcta.

## Agragando las asociaciones

### Asociación 1:N
Analicemos por un segundo como podría ser la relación entre estas dos tablas:
    - Un `customer` tiene un único `address` pero
    - En un `address` pueden existir varios `customers`
Es decir tenemos una relación de uno a muchos `One-To-Many`. Para indicarle a `Sequelize` este tipo de relación o asociación debemos usar la combinación `BelongsTo` y `HasMany`.

<img src="./images/der_1.jpg">

Vamos agregar una nueva función de uso interno (no hay que exportarla) en nuestro `serviceDB.js`, que será invocada al principio de la función `syncBD`:

> `./src/database/services/servicesDB.js`

```javascript
export const syncDB = async () => {
    associationsModels();
    await Actors.sync({alter: true});
    await Customer.sync({alter: true});
    await Address.sync({alter: true});
}

const associationsModels = () => {
    Customer.belongsTo(Address, {foreignKey: 'address_id'});
    Address.hasMany(Customer, {foreignKey: 'address_id'});
}
```
Estas asociaciones reciben dos parámertos: el primero es el modelo con el que nos queremos relacionar; y el segundo es un objeto en donde le especificaremos cual es la clave foránea (`foreignKey`) por la cual se relacionan las dos tablas.

```
Siempre, y no hay excpeción alguna, como regla las asociaciones se deben hacer en `pares`, es decir debemos indicar del ModeloA --> ModeloB y ModeloB --> ModeloA.
```

### Haciendo uso de las asociaciones
Para hacer uso de una asociación simplemente debemos incluirla dentro de la ejecución del `findAll()` o `findByPk()` con haciendo uso de la cláusula `include` como se muestra a continuación:

```javascript
import Customer from "../../database/models/Customer.js";
import Address from "../../database/models/Address.js";

const getAllCustomersController = async (req,res) => {
    try {
        
        const customers = await Customer.findAll({
            include: [
                {model: Address} //incluimos la asociación
            ]
        });

        res.send({
            status: 'ok',
            data: customers
        });
        
    } catch (error) {
        console.log(error);
    }
}

export default getAllCustomersController;
```
Esto nos devolverá los datos asociados entre los modelos desde la base de datos.

```
Aclaración:
    Para poder probar el código anterior, recuerde que antes debe tener un enrrutado (customerRouter.js)
    y su respectivo controlador getAllCustomerController.js, y este ser importado en el archivo server.js y aplicado como middleware de aplicación server.use(customerRouter)
```

### Asociación N:M
Para finalizar veremos las asociaciones que corresponden a las relaciones Many-To-Many (muchos a muchos).
La característica principal de este tipo de relaciones es que se hacen mediante una tercera tabla, llamada tabla intermedia o `pivote`.
Veamos un ejemplo con las siguientes tablas:

<img src="./images/der_2.jpg">

En una película (film) hay muchos actores, pero a su vez un actor puede haber participado de muchas películas.
Como tenemos una relación de muchos a muchos aparece la tabla `film_actor`.
¿Como representar esta asociación con `Sequelize`?, la respuesta a esta pregunta es utilizando la asociación `BelongsToMany` de la siguiente manera:
```javascript
 Actors.belongsToMany(Film, {
        through: 'film_actor', //a través de que tabla debe hacerse (tabla intermedia)
        foreignKey: 'actor_id', //clave foránea de la tabla film que hace referencia a actor
        otherKey: 'film_id', // referencia a la tabla intermedia
        timestamps: false // aclaramos si la tabla pivote tiene o no campos timestamp
});

Film.belongsToMany(Actors, {
        through: 'film_actor',
        foreignKey: 'film_id',
        otherKey: 'actor_id',
        timestamps: false
});
```

### Implementación
Con esta asociación podemos, ahora, buscar una película por su id (`film_id`) y mostrar todos sus datos incluyendo los actores que participaron en ella.
Primero crearemos un nuevo archivo `filmRouter.js`:

> `./src/router`

```javascript
import express from 'express';

import getFilmByIdController from '../controllers/films/getFilmByIdController.js';

const router = express.Router();

router.get('/film/:id', getFilmByIdController);

export default router;
```
Luego creamos el controlador `getFilmByIdController.js`

> `./src/router`

```javascript
import Film from "../../database/models/Film.js";
import Actors from "../../database/models/Actors.js";

const getFilmByIdController = async (req,res) => {
    try {
        
        const { id } = req.params;

        const film = await Film.findByPk(id, {
            include: [
                {model: Actors}
            ]
        });

        res.send({
            status: 'ok',
            data: film
        })
    } catch (error) {
        console.log(error);
    }
}

export default getFilmByIdController;
```
Donde dentro del método `findByPk()` de `Film` incluimos la asociación `include: [{model: Actors}]`.

Por último llamamos a nuestro enrrutador desde `server.js`.

> `./src/server.js`

```javascript
...
import filmRouter from '../src/router/filmRouter.js';
...
server.use(filmRouter);
```

De esta misma forma, podríamos buscar un actor y traer asociadas todas las películas en las que participó.
¿Te anímas?