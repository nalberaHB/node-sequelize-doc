# C.R.U.D.

Para finalizar con esta Boost Academy, veremos como desde `Sequelize`, podemos realizar el resto de las operaciones para completar un `CRUD`.
Recordemos que `CRUD` es el acrónimo de: `Create`, `Read`, `Update`, y `Delete`.
Hasta ahora solo hermos trabajado en el `Read` ejecutando consultas contra la base de datos `sakila`.

## Create

`Sequelize` proporciona el método `create` para crear un nuevo registro en una tabla de la bas de datos.
Su forma de uso es la siguiente:

Supongamos que vamos a crear un nuevo `Actor`, para ello tenemos un modelo en nuestro proyecto que lo representa `./src/database/modeles/Actors.js`.

Antes de empezar, agreguemos en nuestro archivo de servidor el middleware `express.json()`

> `./src/server.js`

```javascript

server.use(express.json());

```
```
Recuerde que este método debe invocarlo antes de la implementación de las rutas
```

**Paso 1**
Vamos a crear una nueva ruta dentro de nuestro enrutado de actores, que implemente un controlador llamado `insertNewActorController`

> `./src/router/actorRouter.js`

```javascript
//...
router.post('/actors/new-actor', insertNewActorController);
//...
```

**Paso 2**
Creemos nuestro controlador en la carpeta `controllers`

> `./src/controllers/actors/insertNewActorController.js`

```javascript
//importamos el modelo con el cual necesitamos trabajar
import Actors from "../../database/models/Actors.js";

const insertNewActorController = async (req,res) => {
    try {
        
        //tomamos los datos que nos envían por body
        const { first_name, last_name } = req.body;

        //haciendo uso del método create de sequelize creamos el nuevo actor
        const newActor = await Actors.create({
            //como los datos recibidos tienen el mismo nombre que los campos en la tabla
            //le pasamos un objeto simplemente con la clave para evitar la sintaxis --> clave: valor
            first_name,
            last_name
        });

        //retornamos al cliente la información en caso de que haya resultado satisfactorio
        res.send({
            status: 'ok',
            message: 'Successfully created actor...',
            data: newActor
        });

    } catch (error) {
        console.log(error);
    }
}

export default insertNewActorController;

```
**Paso 3**
Luego lo importamos en nuestro archivo de rutas

> `./src/router/actorRouter.js`

```javascript
//...
import insertNewActorController from '../controllers/actors/insertNewActorCotroller.js';
//...
```

**Paso 4**
Para poder probar este método, y los restantes, se debe hacer desde `Postman` o el software que utilice para testear endponits de `APIs`, ya que los datos son enviados por el método `POST` y por `body`.

> Ejemplo configuración `postman`

<img src="./images/new-actor-pm.jpg">

Si todo salió bien, nos debería mostrar la siguiente información:

> Respuesta (200)

<img src="./images/rta_new-actor-pm.jpg">

> Le recomiendo investigar también un método muy útil para ciertos casos `findOrCreate()`
> https://sequelize.org/docs/v6/core-concepts/model-querying-finders/#findorcreate


## Update
`Sequelize` proporciona un método estático llamado `update` utilizado para actualizar varios registros a la vez. Este método funciona excatamente igual que el `UPDATE` de `MySQL`, es decir... "NO SE OLVIDEN DEL WHERE!!"
`Update` recibe como parámetros dos objetos:
            - el primer objeto: son los datos a modificar y
            - el segundo objeto: es el `where` con la condición por la cual va a realizar la modificación

Modifiquemos ahora el Actor que creamos en el paso anterior.

**Paso 1**
Agreguemos un nuevo `endpoint` en el enrutado de Actores. Esta vez con el método `PUT` que implemente un controlador llamado `modifyActorByIdCotroller`.

> `./src/router/actorRouter.js`

```javascript
//...
router.put('/actors/modify/:id', modifyActorByIdCotroller);
//...
```

**Paso 2**
Creemos nuestro controlador en la carpeta `controllers`

> `./src/controllers/actors/modifyActorByIdCotroller.js`

```javascript
//importamos el modelo con el cual necesitamos trabajar
import Actors from "../../database/models/Actors.js";

const modifyActorByIdCotroller = async (req,res) => {
    try {
        
        //tomamos los datos que nos envían por body
        const { first_name, last_name } = req.body;
        const { id } = req.params;

        await Actors.update(
            //primer parámetro: el objeto con los datos a actualizar
            {
                first_name,
                last_name,
                last_update: new Date()
            },
            //segundo parámetro el WHERE
            {
                where:{
                    actor_id: id
                }
            }
        );

        //retornamos al cliente la información en caso de que haya resultado satisfactorio
        res.send({
            status: 'ok',
            message: 'Successfully modified actor...',
        });

    } catch (error) {
        console.log(error);
    }
}

export default modifyActorByIdCotroller;
```
**Paso 3**
Luego lo importamos en nuestro archivo de rutas

> `./src/router/actorRouter.js`

```javascript
//...
import modifyActorByIdCotroller from '../controllers/actors/modifyActorByIdCotroller.js';
//...
```

**Paso 4**
Comprobamos que todo esté funcionando correctamente con `Postman`

> Ejemplo configuración `postman`

<img src="./images//modify-actor-pm.jpg">

> Respuesta (200)

<img src="./images/rta_modify-actor-pm.jpg">

## Delete
Veremos ahora como se puede eliminar un registro de una tabla con `Sequelize`.
Para esto, el mismo `Sequelize`, nos proporciona un método llamado `destroy`, que como al igual que el método `DELETE` de `MySQL` no debemos olvidarnos del `WHERE`.

**Paso 1**
Agreguemos un nuevo `endpoint` en el enrutado de Actores. Esta vez con el método `DELETE` que implemente un controlador llamado `deleteActorController`.

> `./src/router/actorRouter.js`

```javascript
//...
router.delete('/actors/:id', deleteActorController);
//...
```

**Paso 2**
Creemos nuestro controlador en la carpeta `controllers`

> `./src/controllers/actors/deleteActorController.js`

```javascript
//importamos el modelo con el cual necesitamos trabajar
import Actors from "../../database/models/Actors.js";

const deleteActorController = async (req,res) => {
    try {
        
        const { id } = req.params;

        await Actors.destroy({
            where:{
                actor_id: id
            }
        });

        res.send({
            status: 'ok',
            message: 'Successfully deleted actor...',
        });

    } catch (error) {
        console.log(error);
    }
}

export default deleteActorController;
```
**Paso 3**
Luego lo importamos en nuestro archivo de rutas

> `./src/router/actorRouter.js`

```javascript
//...
import deleteActorController from '../controllers/actors/deleteActorController.js';
//...
```

**Paso 4**
Comprobamos que todo esté funcionando correctamente con `Postman`

> Ejemplo configuración `postman`

<img src="./images/destroy-actor-pm.jpg">

> Respuesta (200)

<img src="./images/rta_destroy-actor-pm.jpg">