# Object Relational Mapping (ORM)

## ¿Qué es un ORM?

Un ORM (Object Relational Mapping - Mapeo Objeto-Relacional) es un modelo de programación que nos permite transformar las tablas de una base de datos de forma simplificada, es decir nos permite relacionar los objetos con los datos que representan.
Actúa como un puente entre los datos almacenados en una base de datos relacional y le permite a los programadores trabajar con objetos en lugar actuar directamente con las tablas.

<img src="./images/orm-model.jpg">

## Ventajas de uso
Algunas de las principales ventajas 

- Simplifica la interacción con la base de datos: en lugar de escribir código en SQL podemos seguir escribiendo código en el lenguaje que yá conocemos.
- Protabilidad del código: podemos cambiar de un *DBMS* a otro de una forma mucho más simple, el ORM se encargará de adaptar nuestro código y el mapeo de objetos automáticamente. Por ejemlo de MySQL a MariaDB.
- Funciones de seguridad integradas: nos proporncionan medidas de seguridad que ayudan a proteger nuestra aplicación contra ataques malicios, como por ejemplo `"SQL injection"`.
- Mantenibilidad del código: separa la lógica del negocio de los detalles de la base de datos, si se cambian los esquemas de la base de datos, solo tendríamos que actualizar el mapéo de objetos en lugar de modificar manualmente las consultas en nuestro código.

# Sequelize

Sequelize es un `ORM` para Nodejs que nos permite manipular varias bases de datos de manera sencilla, entre ellas: `MySQL`, `MariaDB`, `Sqlite`, `Postgres`, entre otras.
Se utiliza del lado el backend y cuando decimos manipular estamos hablando que nos permite insetar, buscar, actualizar y eliminar datos.
Como recomendación siempre es útil tener a mano la documentación de la tecnología que estamos utilizando, que al momento de escribir esta documentación nos encontramos con la versión 6 de Sequelize que es la última versión estable.
https://sequelize.org/docs/v6/

## Instalación y configuración

Antes de instalar Sequelize, se debe tener presente que al ser un paquete que utiliza NodeJs, debemos tener un proyecto de Node configurado con todo lo que eso implica (dependencias, carpetas, archivos de servicios, servidor configurado, etc.)

### Instalación

En la carpeta del proyecto:
```javascript
npm install sequelize
npm install mysql2
```
Además del resto de dependencias, que suponemos al llegar a estas instancias ya deberían estar instaladas: express, nodemon, morgan, etc...

### Configuración

Dentro de nuestro árbol de directorios, además de una carpeta `controllers`, `router` o `routes`, vamos a crear una carpeta llamada `database` y dentro de ella tres carpetas más, `config`, `models` y `services` todo esto contenido en la carpeta `src`.
Debería verse algo así:
<img src="./images/arbol-carpetas1.jpg">

### Conexión a base de datos

Como se dijo anteriórmente, sequelize es un ORM que nos permite conectarnos a una base de datos MySQL, Postgres, MariaDB, etc, y para esto nos ofrece varias formas o `cadenas de conexión`.
Remitir a la documentación para mayor información: https://sequelize.org/docs/v6/getting-started/

**Paso 1:**
Dentro de la carpeta `config` crearemos un archivo llamado como sugerencia `configDB.js` pero podría llamarse de cualquier forma `conectionDB.js` o `conection.js` lo importante es lo que escribiremos dentro del mismo:

> `./src/database/config/configDB.js`

```javascript
import { Sequelize } from "sequelize";

import dotenv from 'dotenv';

dotenv.config();

const {DB_USER, DB_PASSWORD, DB_HOST, DB_NAME, DB_DIALECT} = process.env;

const connection = new Sequelize(
    DB_NAME,
    DB_USER,
    DB_PASSWORD,
    {
        host: DB_HOST,
        dialect: DB_DIALECT
    }
);

export default connection;
```
Lo primero que debemos hacer es importar Sequelize `import { Sequelize } from "sequelize";`, que será el responsable de generar esa conexión a nuestra base de datos, que como ejemplo usaremos, para este caso, la base de datos `Sakila`, https://dev.mysql.com/doc/sakila/en/sakila-installation.html

La forma de conexión que estamos usando, y de acuerdo a la documentación oficial, se llama `Passing parameters separately` (paso de parámetros por separado)

```javascript
const connection = new Sequelize(
    base_de_datos /*que nos queremos conectar, sakila en nuestro caso*/,
    usuario /*root o el nombre de usuario que tengamos configurado en MySQL*/,
    contraseña /*la que hemos configurado al instalar MySQL caso contrario cadena vacía ''*/,
    {
        host: servidor /*localhost*/,
        dialect: lenguaje /*mysql*/
    }
);
```
```
Nota: todos los parámetros que se le pasan a la conexión son del tipo String
```
Como se puede ver estamos usando variables de `entorno` (debemos tener instalada la dependencia `dotenv` `npm install dotenv`), recordar la importancia del uso de las mismas para no exponer innecesariamente datos `sencibles` como son las credenciales de conexión a nuestas bases de datos entre otros. Estas variables están contenidas en nuestro `.env` dentro de nuestro proyecto:
```.env
DB_USER=root
DB_PASSWORD=1234 #o cadena vacía en el caso que no tengamos contraseña configurada
DB_HOST=localhost
DB_NAME=sakila
DB_DIALECT=mysql
```
```
Nota: NO OLVIDAR EXPORTAR LA FUNCIÓN connection
```

**Paso 2**
`Como segundo paso` trabajaremos ahora dentro de la carpeta `services`, dentro de ella crearemos un archivo llamado, como sugerencia `servicesDB.js`. Dentro de el importatemos nuestra configuración que hicimos previamente y exportaremos una función (asincrónica) llamada `testConnection` que usaremos luego para comprobar que todo lo que hicimos anteriormente funciona en forma correcta:

> `./src/database/services/servicesDB.js`

```javascript
import connection from '../config/configDB.js';

export const testConnection = async () => {
    try {
        await connection.authenticate();
        console.log(`Connected to database ${connection.getDatabaseName()}`);
    } catch (error) {
        console.log(error); 
    }
}
```
**Paso 3**
`Como tercer y último paso`, en nuestro archivo en donde configuramos nuestro servidor `express`, probaremos que la conexión esté funcionando.

> `./index.js`

```javascript
import server from "./src/server.js";
import { testConnection } from "./src/database/services/servicesDB.js";

const PORT = process.env.PORT || 3001;

testConnection().then( () => {
    server.listen(PORT, () => console.log(`Servidor escuchando en puerto: ${PORT}`));
});
```
Desde la consola ejecutamos el script que hayamos agregado dentro del archivo `package.json`, en este caso suponemos que se tiene un script `"dev": "nodemon index.js"`, por lo que se ejecutará el comando `npm run dev`, y si todo está bien configurado, nos mostrará por la consola el siguiente mensaje:

<img src="./images/mensaje-consola-1.jpg">

De esta forma hemos configurado nuestra conexión a la base de datos `sakila`, en caso de haber algún error se nos mostrará por la misma consola.

## Modelos
Los modelos son la esencia en `Sequelize`, un modelo es una abstracción que representa una tabla en nuestra base de datos.
Básicamente son una clase que `extiende` de Model.
Cabe aclarar que un `modelo no contiene lógica` ni describen como pueden presentarse los datos al usuario. Y siempre vamos a `crar un modelo` por cada tabla que tengamos en la base de datos.

## Creando un Modelo

Tomaremos como ejemplo la tabla `actor` de la base de datos `sakila` veremos que está compuesta por los siguientes atributos:

<img src="./images/tabla-actor.jpg">

Y le crearemos el modelo que la represente en nuestro proyecto. Dentro de la carpeta `models` crearemos un archivo con el `mismo` nombre que tiene en la base de datos respetando que la primera letra debe ir en *`"mayusculas"`*, para este caso crearemos un archivo llamado `Actors.js`

> `./src/database/models/Actors.js`

Lo primero será importar `DataTypes` y `Model` de sequelize.
```javascript
import { DataTypes, Model } from "sequelize";
import connection from "../config/configDB.js";
...
```
Luego crearemos una clase con la palabra reservada `class` que tendrá el mismo nombre del archivo y que extiende `extends` de `Model`:

```javascript
...
class Actors extends Model {};
...
```

Luego haciendo uso de la porpiedad `init` de la clase `Actors`, inicializaremos el modelo, respetando los nombres, tipos de datos y propiedades de cada uno de los campos de la tabla y exportandoló al final.
El archivo finalmente debe verse como se muestra a continuación:

```javascript

import { DataTypes, Model } from "sequelize";
import connection from "../config/configDB.js";

class Actors extends Model {};

Actors.init({
    actor_id:{
        type: DataTypes.INTEGER.UNSIGNED,
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    first_name:{
        type: DataTypes.STRING,
        defaultValue: null
    },
    last_name:{
        type: DataTypes.STRING,
        defaultValue: null
    },
    last_update:{
        type: DataTypes.DATE,
        defaultValue: null
    }
    },
    {
        tableName: 'actor',
        timestamps: false,
        sequelize: connection
    }
)

export default Actors;
```
Para ampliar la información de los métodos y formas que existen dentro de sequelize para crear los modelos y los tipos de datos que soprta, le recomiendo hechar un vistazo al siguiente enlace: https://sequelize.org/docs/v6/core-concepts/model-basics/

## Sincronización de modelos (Model synchronization)
Cuando se define un modelo, le estamos diciendo a Sequelize algunas cosas sobre nuestra tabla en la base de datos. Pero..., ¿qué pasa si la tabla ni siquiera existe en la base de datos? o ¿Qué pasaría si existe, pero tiene columnas diferentes, menos columnas o cualquier otra diferencia?

La respuesta a estas preguntas es la sincronización del/los modelo/modelos.
Un modelo se puede sincronizar con la base de datos llamando a `model.sync()`. Se trata de una función asincrónica que devuelve una Promesa. Con esta llamada, Sequelize realizará automáticamente una consulta SQL a la base de datos.
Hay que tener en cuenta que esto cambia solo la tabla en la base de datos, no el modelo en el lado de JavaScript.

### Opciones de sincronización:
```javascript
Model.sync(); //crea la tabla si no existe, caso contrario no hace nada
Model.sync({ force: true }); //crea la tabla, eliminandola si ya existía (OJO con esta opción)
Model.sync({ alter: true }); //verifica el estado actual de la tabla en la BD y luego realiza los cambios necesarios en la tabla para que coincida con el modelo (normalmente la mas recomendada)
```
*Reemplazar la palabra `Model` por el nombre del modelo definido*

### Sincronizar nuestros modelos en el proyecto
Veamos ahora como debemos sincronizar el modelo que tenemos creado hasta el momento en nuestro proyecto.
Para ello volvamos a nuestro archivo `servicesDB.js` e importemos nuestro modelo.

> `./src/database/services/servicesDB.js`

```javascript
import connection from '../config/configDB.js';

import Actors from '../models/Actors.js';
```
Luego, dentro del mismo exportaremos una función asincrónica llamada `syncDB` o `syncModels` o simplemente `synchronization`

> `./src/database/services/servicesDB.js`

```javascript
export const syncDB = async () => {
    
    await Actors.sync({alter: true});
}
```
Dentro de esta función, cada vez que creemos un nuevo modelo, debemos agregarlo aquí, previamente importado. Por el momento el archivo debe quedar así:

> `./src/database/services/servicesDB.js`

```javascript
import connection from '../config/configDB.js';

import Actors from '../models/Actors.js';

export const testConnection = async () => {
    try {
        await connection.authenticate();
        console.log(`Connected to database ${connection.getDatabaseName()}`);
    } catch (error) {
        console.log(error); 
    }
}

export const syncDB = async () => {
    
    await Actors.sync({alter: true});
}
```
Una vez hecho esto, volvamos a nuestro archivo de servidor e invoquemos la función `syncDB`:

> `./index.js`

```javascript
import server from "./src/server.js";
import { syncDB, testConnection } from "./src/database/services/servicesDB.js";

const PORT = process.env.PORT || 3001;

testConnection().then(() => {
    syncDB().then(() => {
        server.listen(PORT, () => {
            console.log(`Server escuchando en puerto: ${PORT}`);
        })
    })
})
```
Al guardar este arhcivo, y si todo salió bien, en la consola ahora veremos esto:

<img src="./images/mesaje-consola-2.jpg">

De esta manera se nos está diciendo, que además de haber realizado la conexíon a la base de datos exitosamente y levantar el servidor de express en el puerto indicado, ahora se ha sincronizado el modelo con la tabla.

## Consultas
Sequelize nos provee de una serie de métodos `find` para realizar búsquedas dentro de la base de datos. Este método nos da algunas variaciones: `findAll()`, `findByPk()` entre otros.

### findAll()
Equivale al `SELECT * FROM tabla` de MySQL, es decir nos devuelve TODOS los registros de la tabla dentro de un array.
Tomando como ejemplo nuestro modelo `Actors`, veamos como podemos devolver un listado con todos los actores que contiene la tabla.
Para ello necesitamos una ruta que cumpla con este propósito. En la carpeta `router` vamos a crear un archivo `actorRouter.js`

> `./src/router/actorRouter.js`

```javascript
import express from 'express';

import getAllActorsController from '../controllers/actors/getAllActorsController.js';

const router = express.Router();

router.get('/actors', getAllActorsController);

export default router;
```
Y por último vamos a crear en nuestra carpeta `controllers` una carpeta llamada `actors` que contendrá todos los controladores que se invoquen desde `actorRouter.js`, dentro de `actors` vamos a crear nuetro controlador `getAllActorsController.js`

> `./src/controllers/actors/getAllActorsController.js`

```javascript
import Actors from '../../database/models/Actors.js';

const getAllActorsController = async (req,res) => {
    try {
        
        const actors = await Actors.findAll();

        res.send({
            status: 'ok',
            data: actors
        });

    } catch (error) {
        console.log(error);
    }
}

export default getAllActorsController;
```
Ahora que pasaría si solo queremos que `findAll()` nos devuelva solo algunos atributos, por ejemplo solamente `first_name` y `last_name`, para ello se le puede pasar un objeto como parámeto con la clave `attributes` de la siguiente manera:

```javascript
const actors = await Actors.findAll({
    attributes: ['first_name','last_name']
});
```
### findByPk()
Nos permite buscar un registro por su clave primaria `primaryKey`.
Agreguemos una nueva ruta en nuestro `actorRouter.js`

> `./src/router/actorRouter.js`

```javascript
router.get('/actors/:id', getActorById);
```

Y luego creemos el archivo `getActorById` en los `controllers`

> `./src/controllers/actors/getActorById.js`

```javascript
import Actors from "../../database/models/Actors.js";

const getActorById = async (req,res) => {
    try {
        
        const { id } = req.params;

        const actor = await Actors.findByPk(id);

        if(!actor) return res.send({
            status: 'error',
            message: 'User not found'
        });

        res.send({
            status: 'ok',
            data: actor
        });

    } catch (error) {
        console.log(error);
    }
}

export default getActorById;
```
```
Para más información sobre los métodos find() visite la documentación oficial
https://sequelize.org/docs/v6/core-concepts/model-querying-finders/
```
## Where
Muchas veces vamos a querer buscar en la base de datos solo aquellos registros que coincidan o cumplan con cierta condición. Para este cometido usamos un objeto literal con el atriburo `WHERE` y un método de búsqueda.
Para agregar una condición simplemente agregaremos el atributo `where` al método `findAll()`.

Modifiquemos nuestro método `getAllActorsController` para que si se le pasa un parámetro `name` por la `query string`, nos devuelva aquellos actores que coincidan con dicho `name`.

```javascript
const getAllActorsController = async (req,res) => {
    try {

        const { name } = req.query;

        if(name){
            const actorsName = await Actors.findAll({
                attributes: ['first_name','last_name'],
                where:{
                    first_name: name
                }
            });

            if(actorsName.length){
                return res.send({
                    status: 'ok',
                    data: actorsName
                });        
            }else{
                return res.send({
                    status: 'error',
                    message: 'User not found'
                })
            }
        }

        const actors = await Actors.findAll({
            attributes: ['first_name','last_name']
        });


        res.send({
            status: 'ok',
            data: actors
        });

    } catch (error) {
        console.log(error);
    }
}

```
De esta manera logramos reutilizar el mísmo método para buscar todos los actores o por un determinado nombre con el uso del `where`.

```
Para más información visite la documentación oficial
https://sequelize.org/docs/v6/core-concepts/model-querying-basics/
```